import React from 'react';
import { TextInput, View } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../../style';

class VTGOTextarea extends React.Component {
  render() {
    let { placeholder, value, onChangeText, style } = this.props
    return (
      <View style={{ ...styles.w_full, border: 0, ...style }}>
        <TextInput
          multiline={true}
          numberOfLines={4}
          placeholder={placeholder}
          placeholderTextColor='#d5d5d5'
          style={{}}
          value={value}
          onChangeText={onChangeText} />
      </View>
    )
  }
}

VTGOTextarea.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChangeText: PropTypes.func
}

VTGOTextarea.defaultProps = {
  placeholder: '',
  value: ''
}

export default VTGOTextarea
