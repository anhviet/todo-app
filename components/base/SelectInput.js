import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { Text } from 'react-native-elements'
import Ionicons from 'react-native-vector-icons/Ionicons'
import RNPickerSelect from 'react-native-picker-select'
import styles from '../../style'

export default class SelectInput extends React.Component {
  render() {
    let { placeholder, style, right, value, data, title, titleStyle } = this.props
    let fixedStyle = {
      width: '100%',
      paddingVertical: 10,
      paddingHorizontal: 0,
      borderBottomWidth: 1,
      borderBottomColor: '#d5d5d5',
      color: 'black',
      paddingRight: 0,
      paddingLeft: 0
    }
    if (!style) {
      style = {}
    }
    let iconContainer = right ? { top: 15, right: 5 } : { top: 15 }
    return (
      <View style={{ width: '100%', ...styles.p_5, ...style }}>
        {
          title
            ? <Text style={{ ...styles.weightgray_txt, ...titleStyle }}>{title}</Text>
            : null
        }
        <RNPickerSelect
          placeholder={{ label: placeholder ? placeholder : '', value: null }}
          items={data}
          onValueChange={(sltItem) => this.props.onSelect(sltItem)}
          style={{
            inputIOS: Object.assign({}, fixedStyle, style),
            inputAndroid: Object.assign({}, fixedStyle, style),
            iconContainer: iconContainer
          }}
          value={value}
          Icon={() => <Ionicons name='md-arrow-dropdown' size={20} color='gray' />}
        />
      </View>
    )
  }
}

SelectInput.propTypes = {
  style: PropTypes.object,
  titleStyle: PropTypes.object,
  title: PropTypes.string,
  placeholder: PropTypes.string,
  onSelect: PropTypes.func
}

SelectInput.defaultProps = {
  style: {},
  titleStyle: {},
  title: null,
  placeholder: ''
}