import React from 'react';
import PropTypes from 'prop-types'
import { Button } from 'react-native-elements'
import styles from '../../style'

class VTGOButton extends React.Component {
  render() {
    let { title, width, disabled, rounded, type, onPress } = this.props
    let buttonStyle = {}
    if (width) {
      buttonStyle.width = width
    }
    switch (type) {
      case 'reject':
        buttonStyle = Object.assign(buttonStyle, { ...styles.btn, ...styles.red_bg, ...styles.mb_10 })
        break
      case 'confirm':
        buttonStyle = Object.assign(buttonStyle, { ...styles.btn, ...styles.blue_bg, ...styles.mb_10 })
        break
      case 'success':
        buttonStyle = Object.assign(buttonStyle, { ...styles.btn, ...styles.green_bg, ...styles.mb_10 })
        break
      default:
        buttonStyle = Object.assign(buttonStyle, { ...styles.btn, ...styles.lightgray_bg, ...styles.mb_10 })
    }
    return (
      <Button
        disabled={disabled}
        title={title}
        titleStyle={{ ...styles.white_txt, ...styles.bold, ...styles.center_txt, ...styles.normal_txt }}
        buttonStyle={
          rounded
            ? { ...buttonStyle, ...styles.br_30, ...styles.mt_20 }
            : { ...buttonStyle, ...styles.br_10, ...styles.mt_20 }}
        onPress={onPress} />
    )
  }
}

VTGOButton.propTypes = {
  disabled: PropTypes.bool,
  type: PropTypes.string,
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  rounded: PropTypes.bool
}

VTGOButton.defaultProps = {
  type: 'success',
  rounded: true,
  disabled: false
}

export default VTGOButton