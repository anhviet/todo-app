import React from 'react'
import PropTypes from 'prop-types'
import Swipeable from 'react-native-swipeable'

export default class VTGOSwipeable extends React.Component {
  render() {
    const { leftButtons, rightButtons, onSwipeStart, onSwipeRelease } = this.props
    return (
      <Swipeable
        onSwipeStart={onSwipeStart}
        onSwipeRelease={onSwipeRelease}
        rightButtons={rightButtons}
      >
        {this.props.children}
      </Swipeable>
    )
  }
}

VTGOSwipeable.propTypes = {
  onSwipeStart: PropTypes.func,
  onSwipeRelease: PropTypes.func,
  leftButtons: PropTypes.arrayOf(PropTypes.element),
  rightButtons: PropTypes.arrayOf(PropTypes.element)
}

VTGOSwipeable.defaultProps = {
  leftButtons: [],
  rightButtons: []
}