import React from 'react';
import PropTypes from 'prop-types'
import { View, Keyboard } from 'react-native'
import { Text, Input } from 'react-native-elements'
import colors from '../../constants/colors';
import styles from '../../style';

class VTGOInput extends React.Component {
  state = {
    selection: {
      start: 0,
      end: 0
    }
  }

  onChangeText = (text) => {
    const lastKey = text.slice(-1)
    let newText = text
    switch (this.props.keyboardType) {
      case 'phone-pad':
      case 'numeric':
        if (['-', '.', '*', '(', ')', 'N', '/', ',', '#', '_', ';', '+', ' '].indexOf(lastKey) !== -1) {
          newText = newText.substring(0, newText.length - 1)
        }
        break
    }
    this.props.onChangeText(newText)
  }

  render() {
    let { keyboardType, noBorder, secure, titleStyle, title, placeholder, value, leftIcon, rightIcon, disabled, style, readonly } = this.props
    return (
      <View style={{...styles.w_full}}>
        {title ? <Text style={{ ...styles.weightgray_txt, ...titleStyle, ...styles.ml_10 }}>{title}</Text> : null}
        {
          readonly ?
            <Input
              keyboardType={keyboardType ? keyboardType : 'default'}
              onFocus={() => {
                if (readonly) {
                  Keyboard.dismiss()
                  this.setState({
                    selection: {
                      start: 0,
                      end: 0
                    }
                  })
                }
              }}
              selection={this.state.selection}
              editable={!disabled}
              secureTextEntry={secure}
              placeholderTextColor={colors.GRAY}
              placeholder={placeholder}
              value={value}
              inputStyle={Object.assign({ ...styles.normal_txt }, style ? style : {})}
              inputContainerStyle={noBorder ? { borderWidth: 0 } : { ...styles.gray_border_b }}
              leftIcon={leftIcon}
              rightIcon={rightIcon}
              onChangeText={this.onChangeText} />
            :
            <Input
              keyboardType={keyboardType ? keyboardType : 'default'}
              editable={!disabled}
              secureTextEntry={secure}
              placeholderTextColor={colors.GRAY}
              placeholder={placeholder}
              value={value}
              inputStyle={Object.assign({ ...styles.normal_txt }, style ? style : {})}
              inputContainerStyle={noBorder ? { borderBottomWidth: 0 } : { ...styles.gray_border_b }}
              leftIcon={leftIcon}
              rightIcon={rightIcon}
              onChangeText={this.onChangeText} />
        }
      </View>
    )
  }
}

VTGOInput.propTypes = {
  keyboardType: PropTypes.oneOf(['default', 'number-pad', 'decimal-pad', 'numeric', 'email-address', 'phone-pad']),
  readonly: PropTypes.bool,
  disabled: PropTypes.bool,
  secure: PropTypes.bool,
  titleStyle: PropTypes.object,
  title: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  rightIcon: PropTypes.object,
  leftIcon: PropTypes.object,
  noBorder: PropTypes.bool,
  onChangeText: PropTypes.func
}

VTGOInput.defaultProps = {
  keyboardType: 'default',
  readonly: false,
  disabled: false,
  secure: false,
  titleStyle: {},
  title: null,
  placeholder: '',
  rightIcon: null,
  leftIcon: null,
  noBorder: false
}

export default VTGOInput