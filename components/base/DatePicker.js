import React from 'react';
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { Text } from 'react-native-elements'
import DatePicker from 'react-native-datepicker'
import styles from '../../style';
import moment from 'moment'
import AppConfig from '../../config'

class VTGODatePicker extends React.Component {
  state = {
    date: moment().format(this.props.type === 'date' ? AppConfig.FORMAT_DATE : AppConfig.FORMAT_TIME)
  }

  sltDate = (date) => {
    this.setState({
      date: date
    }, () => {
      this.props.onDateChange(date)
    })
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      date: moment(nextProps.initValue, AppConfig.FORMAT_DATE_TIME).format(nextProps.type === 'date' ? AppConfig.FORMAT_DATE : AppConfig.FORMAT_TIME)
    })
  }

  render() {
    let { titleStyle, title, placeholder, style, type, iconComponent, limit } = this.props
    return (
      <View style={{ ...styles.w_full, ...styles.mt_10, ...styles.mb_10, ...style }}>
        {
          title
            ? <Text style={{ ...styles.ml_10, ...styles.weightgray_txt, ...titleStyle }}>{title}</Text>
            : null
        }
        <DatePicker
          style={{ ...styles.w_full, ...styles.pl_10, ...styles.pr_10 }}
          date={this.state.date}
          mode={type}
          placeholder={placeholder}
          format={type === 'date' ? AppConfig.FORMAT_DATE : AppConfig.FORMAT_TIME}
          minDate={type === 'date' && limit ? moment().format(AppConfig.FORMAT_DATE) : undefined}
          confirmBtnText='Chọn'
          cancelBtnText='Huỷ'
          iconComponent={iconComponent}
          customStyles={{
            dateInput: {
              borderWidth: 0,
              ...styles.border_b_w_1,
              ...styles.gray_border_b,
              alignItems: 'flex-start'
            },
            dateIcon: {
              position: 'absolute',
              right: 5
            }
          }}
          onDateChange={(date) => this.sltDate(date)}
        />
      </View>
    )
  }
}

VTGODatePicker.propTypes = {
  initValue: PropTypes.string,
  limit: PropTypes.bool,
  type: PropTypes.string,
  titleStyle: PropTypes.object,
  title: PropTypes.string,
  placeholder: PropTypes.string,
  date: PropTypes.string,
  iconComponent: PropTypes.object,
  onDateChange: PropTypes.func
}

VTGODatePicker.defaultProps = {
  initValue: null,
  limit: true,
  type: 'date',
  titleStyle: {},
  title: null,
  placeholder: '',
  date: '',
  iconComponent: null
}

export default VTGODatePicker