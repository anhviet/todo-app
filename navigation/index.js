import React from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import Root from './Root'

class AppRoutes extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Root />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

export default connect(mapStateToProps, false)(AppRoutes)