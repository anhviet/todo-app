import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import RoutesInApp from './App';

export default createAppContainer(createSwitchNavigator({
	Main: RoutesInApp
}, {
		initialRouteName: 'Main'
	})
)