import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
import TodoListScreen from '../screens/App/TodoList'
import TodoDetailScreen from '../screens/App/TodoDetail'

const TodoList = createStackNavigator({
  TodoList: {
    screen: TodoListScreen
  },
  TodoDetail: {
    screen: TodoDetailScreen
  }
}, {
    initialRouteName: 'TodoList'
  }
)

// All Routes
export default createDrawerNavigator({
  TodoList: {
    screen: TodoList
  }
}, {
    initialRouteName: 'TodoList'
  }
);
