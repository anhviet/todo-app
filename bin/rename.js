const fs = require('fs-extra');
const readline = require('readline');
const replace = require('replace-in-file');

const BASE_DIRECTORY = './';
const VALID_CHARACTERS = /^[a-zA-Z\s]+$/;
const DEFAULT_PACKAGE_NAME = 'com.company.boilerplate';

// Get information of project
var CURRENT_PACKAGE_NAME = DEFAULT_PACKAGE_NAME
var PROJECT_NAME = CURRENT_PACKAGE_NAME.split('.')[2] //boilerplate
var COMPANY_NAME = CURRENT_PACKAGE_NAME.split('.')[1] //company

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const readInput = (input) => {
  return new Promise((resolve, reject) => {
    rl.question(`Enter your ${input}: `, (answer) => {
      resolve(answer);
    })
  })
};

const replaceInFile = (from, to) => {
  return new Promise((resolve, reject) => {
    const options = {
      files: [
        './android/**',
        './ios/**',
        './*',
      ],
      from: new RegExp(from, 'g'),
      to: to
    };
    replace(options)
    .then(changedFiles => {
      if (changedFiles) {
        console.log('[replaceInFile] Modified files: \n', changedFiles.join('\n'));
      }
      resolve();
    })
    .catch(error => {
      console.error('[replaceInFile] Error occurred: ', error);
      reject(error);
    })
  })
};

const renameFiles = (dir, from, to) => {
  const files = fs.readdirSync(dir);
  for (let i = 0; i < files.length; i += 1) {
    const filename = files[i];
    const path = dir + '/' + filename;
    const file = fs.statSync(path);
    let newPath;
    if (filename.indexOf(from) !== -1) {
      newPath = dir + '/' + filename.replace(from, to);
      fs.renameSync(path, newPath);
      console.log(`[renameFiles] Renamed: ${path} to: ${newPath}`);
    }
    // Recursive
    if (file.isDirectory()) {
      renameFiles(newPath || path, from, to);
    }
  }
};

const updateProjectName = (name) => {
  console.log('---------------------------------------');
  console.log(`Updating project name: ${name}`);
  console.log('---------------------------------------');
  return replaceInFile(PROJECT_NAME, name)
  .then(() => {
    console.log('---------------------------------------');
    console.log('Finished updating project name');
    console.log('---------------------------------------');
    console.log();
  });
};

const updatePackageName = (packageName) => {
  console.log('---------------------------------------');
  console.log(`Updating package name: ${packageName}`);
  console.log('---------------------------------------');
  return replaceInFile(CURRENT_PACKAGE_NAME, packageName)
  .then(() => {
    console.log('---------------------------------------');
    console.log('Finished updating package name');
    console.log('---------------------------------------');
    console.log();
  });
  ;
};

const renameProjectFiles = (name) => {
  console.log('---------------------------------------');
  console.log(`Rename project files`);
  console.log('---------------------------------------');
  return new Promise((resolve, reject) => {
    renameFiles(BASE_DIRECTORY, PROJECT_NAME, name);
    renameFiles(BASE_DIRECTORY, PROJECT_NAME.toLowerCase(), name.toLowerCase());
    console.log('---------------------------------------');
    console.log('Finished renaming project files');
    console.log('---------------------------------------');
    console.log();
    resolve();
  })
};

const renameCompanyFiles = (name) => {
  console.log('---------------------------------------');
  console.log(`Rename company files`);
  console.log('---------------------------------------');
  return new Promise((resolve, reject) => {
    renameFiles(BASE_DIRECTORY, COMPANY_NAME, name);
    console.log('---------------------------------------');
    console.log('Finished renaming company files');
    console.log('---------------------------------------');
    console.log();
    resolve();
  })
};

const run = async () => {
  console.log('---------------------------------------------------------');
  let currentPackage = await readInput('Current project name (com.company.boilerplate)');
  if (!currentPackage || currentPackage === '' || currentPackage.trim() === '') {
    currentPackage = CURRENT_PACKAGE_NAME
  }
  console.log('Old current company is: ', currentPackage.split('.')[1]);
  console.log('Old current project is: ', currentPackage.split('.')[2]);
  console.log('---------------------------------------------------------');

  console.log('---------------------------------------------------------');
  let projectName = await readInput('New project name');
  console.log('---------------------------------------------------------');
  if (!projectName || projectName === '' || projectName.trim() === '') {
    throw new Error('ERROR: Please supply a project name');
  }
  if (!projectName.match(VALID_CHARACTERS)) {
    throw new Error('ERROR: The project name must only contain letters or spaces');
  }

  let companyName = await readInput('New company name');
  console.log('---------------------------------------------------------');
  if (!companyName || companyName === '' || companyName.trim() === '') {
    throw new Error('ERROR: Please supply a company name');
  }
  if (!companyName.match(VALID_CHARACTERS)) {
    throw new Error('ERROR: The company name must only contain letters or spaces');
  }

  // Old information
  CURRENT_PACKAGE_NAME = currentPackage
  PROJECT_NAME = CURRENT_PACKAGE_NAME.split('.')[2]
  COMPANY_NAME = CURRENT_PACKAGE_NAME.split('.')[1]
  // New information
  projectName = projectName.replace(/ /g, '');
  companyName = companyName.replace(/ /g, '').toLowerCase();
  const newPackageName = `com.${companyName}.${projectName.toLowerCase()}`;

  // Close the input
  rl.close();

  updateProjectName(projectName)
  .then(() => updatePackageName(newPackageName))
  .then(() => renameProjectFiles(projectName))
  .then(() => renameCompanyFiles(companyName))
  .then(() => {
    console.log();
    console.log('---------------------------------------------------------');
    console.log('Set project parameters to:');
    console.log('---------------------------------------------------------');
    console.log('Project name: ', projectName);
    console.log('Company name: ', companyName);
    console.log('Package name: ', newPackageName);
    console.log('---------------------------------------------------------');
    console.log();
  });
};

run().catch((error) => {
  console.error(error.message);
  console.log('---------------------------------------------------------');
  process.exit();
});
