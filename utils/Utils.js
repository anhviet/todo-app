export const convertDurationTime = (time) => {
  if (time <= 0) {
    return 'Expired'
  }
  let days = time / (24 * 3600 * 1000)
  if (days >= 1) {
    return `${Math.floor(days).toString()} days`
  }
  let hours = Math.floor(time / (3600 * 1000))
  let mins = Math.floor((time - hours * 3600 * 1000) / (60 * 1000))
  let seconds = Math.floor((time - hours * 3600 * 1000 - mins * 60 * 1000) / 1000)
  if(hours === 0) {
    if(mins === 0) {
      return `${seconds} s`
    } else {
      return `${mins}m ${seconds}s`
    }
  } else {
    return `${hours}h ${mins}m`
  }
}