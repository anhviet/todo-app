import { openDatabase } from 'react-native-sqlite-storage';
window.db = openDatabase({ name: 'todoapp.db' })

window.db.transaction((query) => {
  query.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='cards'", [], (tx, res) => {
    // query.executeSql('DROP TABLE IF EXISTS cards', [])
    if (!res.rows.length) {
      query.executeSql('CREATE TABLE IF NOT EXISTS cards(id INTEGER, title VARCHAR(255), description VARCHAR(255), date VARCHAR(255), time VARCHAR(255), priority INT(10), status INT(10))', [])
    }
  })
})

export const fetch = () => {
  return new Promise((resolve, reject) => {
    window.db.transaction((query) => {
      query.executeSql('SELECT * FROM cards', [], (tx, results) => {
        console.log('fetch from db', results.rows.item(0))
        let temp = []
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        resolve(temp)
      })
    })
  })
}

export const add = (card) => {
  return new Promise((resolve, reject) => {
    window.db.transaction((query) => {
      const { id, title, description, date, time, priority, status } = card
      query.executeSql('INSERT INTO cards (id, title, description, date, time, priority, status) VALUES (?, ?, ?, ?, ?, ?, ?)', [id, title, description, date, time, priority, status], (tx, results) => {
        console.log('add to db', results)
        if (results.rowsAffected) {
          return resolve(true)
        }
        return reject()
      })
    })
  })
}

export const update = (card) => {
  return new Promise((resolve, reject) => {
    window.db.transaction((query) => {
      const { id, title, description, date, time, priority, status } = card
      query.executeSql('UPDATE cards set title=?, description=? , date=? , time=? , priority=? , status=? where id=?', [title, description, date, time, priority, status, id], (tx, results) => {
        console.log('update to db', results)
        if (results.rowsAffected) {
          return resolve(true)
        }
        return reject()
      })
    })
  })
}

export const clear = async (id) => {
  return new Promise((resolve, reject) => {
    window.db.transaction((query) => {
      query.executeSql('DELETE FROM  cards where id=?', [id], (tx, results) => {
        console.log('delete from db', results)
        if (results.rowsAffected) {
          return resolve(true)
        }
        return reject()
      })
    })
  })
}