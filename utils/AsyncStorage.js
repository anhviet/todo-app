import { AsyncStorage } from 'react-native'

export const fetch = async () => {
  try {
    const tmp = await AsyncStorage.getItem('@data')
    const cards = JSON.parse(tmp)
    if (!cards) return []
    return cards
  } catch (e) {
    return []
  }
}

export const clearAll = async () => {
  try {
    await AsyncStorage.removeItem('@data')
    return true
  } catch (e) {
    return false
  }
}

export const clear = async (id) => {
  try {
    let oldCards = await fetch()
    let newCards = oldCards.filter((c) => c.id !== id)
    await AsyncStorage.setItem('@data', JSON.stringify(newCards))
    return true
  } catch (e) {
    return false
  }
}

export const add = async (card) => {
  try {
    let cards = await fetch()
    cards.push(card)
    await AsyncStorage.setItem('@data', JSON.stringify(cards))
    return true
  } catch (e) {
    return false
  }
}

export const update = async (card) => {
  try {
    let cards = await fetch()
    let found = cards.findIndex(c => c.id === card.id)
    if (found === -1) {
      return false
    }
    cards[found] = card
    await AsyncStorage.setItem('@data', JSON.stringify(cards))
    return true
  } catch (e) {
    return false
  }
}