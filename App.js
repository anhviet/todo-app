import React from 'react';
import { Platform, View, StatusBar } from 'react-native';
import { Provider } from 'react-redux'
import AppRoutes from './navigation';
import { SafeAreaView } from 'react-navigation'
import store from './store'
import styles from './style'

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  async componentDidMount() {
  }

  render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, ...styles.white_bg }}>
            {Platform.OS === 'ios' && <StatusBar barStyle='default' />}
            <AppRoutes />
          </View>
        </SafeAreaView>
      </Provider>
    );
  }
}