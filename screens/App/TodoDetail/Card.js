import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import { View, ScrollView, StyleSheet } from 'react-native'
import Octicons from 'react-native-vector-icons/Octicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { Text } from 'react-native-elements'
import styles from '../../../style'
import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import VBase from '../../../components/base'
import AppConfig from '../../../config'

const Title = (props) => {
  const { title, status, onTitleChange, onStatusChange, isUpdating } = props
  return (
    <View style={cardStyle.title}>
      <View style={{ ...styles.row, ...styles.cross_center }}>
        <View style={{ ...styles.row, ...styles.cross_center }}>
          <FontAwesome name='tasks' size={dimensions.SMALL} color={colors.GRAY} style={{ ...styles.mr_5 }} />
          <Text style={{ ...styles.gray_txt }}>Name</Text>
        </View>
        {
          isUpdating ?
            <VBase.SelectInput
              style={{ width: 120, marginLeft: 'auto' }}
              placeholder=''
              data={[{ value: 0, label: 'Open' }, { value: 1, label: 'Inprogress' }, { value: 2, label: 'Close' }]}
              onSelect={onStatusChange}
              value={status}
              rightIcon={<Ionicons name='md-arrow-dropdown' size={dimensions.LARGE} color={colors.WEIGHT_GRAY} />} />
            :
            null
        }
      </View>
      <VBase.Input
        placeholder="Add task's name"
        noBorder={true}
        value={title}
        style={{ ...styles.bold, ...styles.large_txt }}
        onChangeText={onTitleChange} />
    </View>
  )
}

const Description = (props) => {
  const { description, onDescriptionChange } = props
  return (
    <View style={cardStyle.description}>
      <View style={{ ...styles.row, ...styles.cross_center }}>
        <Octicons name='note' size={dimensions.NORMAL} color={colors.GRAY} style={{ ...styles.mr_5 }} />
        <Text style={{ ...styles.gray_txt }}>Description</Text>
      </View>
      <VBase.Textarea
        placeholder="Add task's description"
        style={{ ...styles.pt_10 }}
        value={description}
        onChangeText={onDescriptionChange} />
    </View>
  )
}

const DateTime = (props) => {
  const { onDateChange, onTimeChange, datetime } = props
  return (
    <View style={cardStyle.time}>
      <View style={{ ...styles.row, ...styles.cross_center }}>
        <MaterialIcons name='access-time' size={dimensions.NORMAL} color={colors.GRAY} style={{ ...styles.mr_5 }} />
        <Text style={{ ...styles.gray_txt }}>Due time</Text>
      </View>
      <View style={{ ...styles.mt_5, ...styles.row, justifyContent: 'space-between' }}>
        <VBase.DatePicker
          initValue={datetime}
          placeholder='Day/ Month/ Year'
          style={{ width: 200 }}
          onDateChange={onDateChange}
          titleStyle={{ ...styles.bold, ...styles.black_txt }}
          iconComponent={<AntDesign name='calendar' size={dimensions.LARGE} color={colors.WEIGHT_GRAY} />} />
        <VBase.DatePicker
          initValue={datetime}
          placeholder='Time'
          type='time'
          style={{ width: 100 }}
          onDateChange={onTimeChange}
          titleStyle={{ ...styles.bold, ...styles.black_txt }}
          iconComponent={<Ionicons name='md-arrow-dropdown' size={dimensions.LARGE} color={colors.WEIGHT_GRAY} />} />
      </View>
    </View>
  )
}

const Priority = (props) => {
  const { priority, onPriorityChange } = props
  return (
    <View style={cardStyle.priority}>
      <View style={{ ...styles.row, ...styles.cross_center }}>
        <FontAwesome name='level-up' size={dimensions.NORMAL} color={colors.GRAY} style={{ ...styles.mr_5 }} />
        <Text style={{ ...styles.gray_txt }}>Priority</Text>
      </View>
      <VBase.SelectInput placeholder='Choose priority'
        data={[{ value: 0, label: 'Low' }, { value: 1, label: 'Medium' }, { value: 2, label: 'High' }]}
        onSelect={onPriorityChange}
        value={priority}
        rightIcon={<Ionicons name='md-arrow-dropdown' size={dimensions.LARGE} color={colors.WEIGHT_GRAY} />} />
    </View>
  )
}

class Card extends React.Component {
  state = {
    id: Math.random().toString(),
    title: '',
    description: '',
    date: moment().format(AppConfig.FORMAT_DATE),
    time: moment().format(AppConfig.FORMAT_TIME),
    priority: 1, //0: Low, 1: Medium, 2: High
    status: 0, //0: Open, 1: Inprogress, 2: Close
    isUpdating: false
  }

  componentDidMount() {
    const { id, title, description, date, time, priority, status } = this.props.init
    if (!title) return
    this.setState({
      id,
      title,
      description,
      date: moment(`${date} ${time}`, AppConfig.FORMAT_DATE_TIME).format(AppConfig.FORMAT_DATE),
      time: moment(`${date} ${time}`, AppConfig.FORMAT_DATE_TIME).format(AppConfig.FORMAT_TIME),
      priority,
      status,
      isUpdating: true
    })
  }

  onTitleChange = (title) => {
    this.setState({
      title
    })
  }

  onStatusChange = (status) => {
    this.setState({
      status
    })
  }

  onDescriptionChange = (description) => {
    this.setState({
      description
    })
  }

  onDateChange = (date) => {
    this.setState({
      date
    })
  }

  onTimeChange = (time) => {
    this.setState({
      time
    })
  }

  onPriorityChange = (priority) => {
    this.setState({
      priority
    })
  }

  onFinish = () => {
    this.props.onUpdate(this.state)
  }

  render() {
    const { title, description, priority, date, time, status, isUpdating } = this.state
    return (
      <View style={cardStyle.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* Name */}
          <Title
            isUpdating={isUpdating}
            title={title}
            status={status}
            onStatusChange={this.onStatusChange}
            onTitleChange={this.onTitleChange} />
          {/* Description */}
          <Description
            description={description}
            onDescriptionChange={this.onDescriptionChange} />
          {/* Time */}
          <DateTime
            onDateChange={this.onDateChange}
            onTimeChange={this.onTimeChange}
            datetime={`${date} ${time}`} />
          {/* Priority */}
          <Priority
            priority={priority}
            onPriorityChange={this.onPriorityChange} />
          {/* Button */}
          <View style={cardStyle.btnFinish}>
            <VBase.Button title='Finish' rounded={false} width={150} onPress={this.onFinish} disabled={!title}/>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Card

Card.propTypes = {
  init: PropTypes.object,
  onUpdate: PropTypes.func
}

const cardStyle = StyleSheet.create({
  container: {
    flex: 1,
    ...styles.p_20,
    ...styles.m_10,
    borderRadius: 5,
    ...styles.white_bg,
    borderBottomWidth: 0,
    shadowColor: '#d5d5d5',
    shadowOffset: {
      width: 2,
      height: 3
    },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    elevation: 1
  },
  title: {
    flex: 1,
    ...styles.border_b_w_1,
    borderBottomColor: '#f3f3f3'
  },
  description: {
    ...styles.pt_20,
    ...styles.pb_20,
    ...styles.border_b_w_1,
    borderBottomColor: '#f3f3f3'
  },
  time: {
    ...styles.pt_20,
    ...styles.pb_20,
    ...styles.border_b_w_1,
    borderBottomColor: '#f3f3f3'
  },
  priority: {
    ...styles.pt_20,
    ...styles.pb_20
  },
  btnFinish: {
    ...styles.row,
    ...styles.main_center
  }
})
