import React from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import Header from './Header'
import styles from '../../../style'
import Card from './Card'
import * as actions from '../../../actions'
import providers from '../../../providers'

class TodoDetailScreen extends React.Component {
  static navigationOptions = (props) => {
    return {
      header: <Header title='Hey you, This is your to-do list.' onGoBack={() => props.navigation.navigate('TodoList')} />
    }
  }

  onUpdate = async (initData, card) => {
    const { id } = initData
    if (id) {
      const ret = await providers.card.update(card)
      if (ret) {
        this.props.updateCard(ret)
      }
    } else {
      const ret = await providers.card.add(card)
      if (ret) {
        this.props.addCard(ret)
      }
    }
    this.props.navigation.navigate('TodoList')
  }

  render() {
    let initData = {}
    if (this.props.navigation.state && this.props.navigation.state.params) {
      initData = this.props.navigation.state.params.data
    }
    return (
      <View style={{ flex: 1, ...styles.lightgray_bg }}>
        <Card init={initData} onUpdate={(card) => this.onUpdate(initData, card)} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addCard: (card) => dispatch(actions.addCard(card)),
    updateCard: (card) => dispatch(actions.updateCard(card))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoDetailScreen)