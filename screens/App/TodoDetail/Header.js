import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { Text } from 'react-native-elements'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import dimensions from '../../../constants/dimensions';
import styles from '../../../style';
import colors from '../../../constants/colors';

class Header extends React.Component {
  goBack = () => {
    this.props.onGoBack()
  }

  render() {
    let { title, subtitle } = this.props
    let titleSize = !subtitle ? styles.large_txt : styles.normal_txt
    return (
      <View style={{ ...styles.row, ...styles.cross_center, height: 60, ...styles.lightgray_bg }}>
        <View style={{ position: 'absolute', left: 5, top: 15, zIndex: 999 }}>
          <SimpleLineIcons
            name='arrow-left'
            size={dimensions.LARGE}
            style={{ ...styles.p_5 }}
            color={colors.BLACK}
            onPress={this.goBack} />
        </View>
        <View style={{ ...styles.view_center, ...styles.w_full }}>
          <Text style={{ ...styles.bold, ...styles.black_txt, ...titleSize }}>{title}</Text>
          {
            subtitle ?
              <Text style={{ ...styles.small_txt }}>{subtitle}</Text>
              :
              null
          }
        </View>
      </View>
    )
  }
}

export default Header

Header.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  onGoBack: PropTypes.func
}

Header.defaultProps = {
  title: null,
  subtitle: null
}
