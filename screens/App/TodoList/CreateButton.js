import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import styles from '../../../style'
import colors from '../../../constants/colors'

class CreateButton extends React.Component {
  render() {
    const { onPress } = this.props
    return (
      <Button
        containerStyle={buttonStyle.container}
        buttonStyle={buttonStyle.component}
        icon={
          <Icon
            type='ionicon'
            name='ios-add'
            size={35}
            color={colors.WHITE}
          />
        }
        onPress={onPress} />
    )
  }
}

export default CreateButton

CreateButton.propTypes = {
  onPress: PropTypes.func
}

const buttonStyle = StyleSheet.create({
  container: {
    zIndex: 999,
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  component: {
    width: 50,
    height: 50,
    borderRadius: 30,
    ...styles.m_15,
    ...styles.green_bg
  }
})
