import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import Octicons from 'react-native-vector-icons/Octicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { Text } from 'react-native-elements'
import styles from '../../../style'
import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import AppConfig from '../../../config'
import * as utils from '../../../utils/Utils'

const priorityText = ['Low', 'Medium', 'High']
const statusText = ['Open', 'Inprogress', 'Close']
const priorityColor = {
  'Low': colors.GREEN,
  'Medium': colors.YELLOW,
  'High': colors.RED
}
const statusColor = {
  'Open': colors.GREEN,
  'Inprogress': colors.YELLOW,
  'Close': colors.RED
}

const Header = (props) => {
  const { title, status } = props
  return (
    <View style={cardStyle.title}>
      <Text style={{ marginRight: 'auto', ...styles.gray_txt }}>{title}</Text>
      <View style={{ marginLeft: 'auto', ...styles.row, ...styles.cross_center }}>
        <Octicons name='primitive-dot' size={dimensions.LARGE} color={statusColor[statusText[status]]} style={{ ...styles.mr_5 }} />
        <Text style={{ ...styles.pl_5, ...styles.bold, ...styles.white_txt, ...styles.pr_5, borderRadius: 3, backgroundColor: statusColor[statusText[status]] }}>{statusText[status]}</Text>
      </View>
    </View>
  )
}

const Body = (props) => {
  const { description } = props
  return (
    <View style={cardStyle.body}>
      <Text style={{ ...styles.bold, ...styles.large_txt, ...styles.black_txt }}>{description}</Text>
    </View>
  )
}

class Footer extends React.Component {
  state = {
    remainingTime: 0,
    count: null
  }

  clearCountdown = () => {
    if (this.state.count) {
      clearInterval(this.state.count)
    }
  }

  countdown = (date) => {
    this.clearCountdown()
    let remainingTime = moment(date, AppConfig.FORMAT_DATE_TIME).valueOf() - moment().valueOf()
    if (remainingTime <= 0) {
      this.setState({
        remainingTime: utils.convertDurationTime(remainingTime)
      })
      return
    }
    this.state.count = setInterval(() => {
      remainingTime = remainingTime - 1 * 1000
      if (remainingTime <= 0) {
        this.setState({
          remainingTime: utils.convertDurationTime(remainingTime)
        })
        this.clearCountdown()
        return
      }
      this.setState({
        remainingTime: utils.convertDurationTime(remainingTime)
      })
    }, 1000)
  }
  
  componentDidMount() {
    this.countdown(this.props.date)
  }

  componentWillReceiveProps(nextProps) {
    this.countdown(nextProps.date)
  }

  componentWillUnmount() {
    this.clearCountdown()
  }

  render() {
    const { priority } = this.props
    return (
      <View style={cardStyle.footer}>
        <Text style={{ marginRight: 'auto', ...styles.bold, ...styles.white_txt, ...styles.pl_5, ...styles.pr_5, borderRadius: 3, backgroundColor: priorityColor[priorityText[priority]] }}>{priorityText[priority]}</Text>
        <View style={{ marginLeft: 'auto', ...styles.row, ...styles.cross_center }}>
          <MaterialIcons name='access-time' size={dimensions.NORMAL} color={colors.GRAY} style={{ ...styles.mr_5 }} />
          <Text style={{ ...styles.gray_txt }}>{this.state.remainingTime}</Text>
        </View>
      </View>
    )
  }
}

class Card extends React.Component {
  render() {
    const { title, status, description, priority, date, time, onCardClick } = this.props
    return (
      <TouchableOpacity onPress={onCardClick}>
        <View style={{ ...cardStyle.container, borderLeftColor: priorityColor[priorityText[priority]] }}>
          <Header title={title} status={status} />
          <Body description={description} />
          <Footer priority={priority} date={`${date} ${time}`} />
        </View>
      </TouchableOpacity>
    )
  }
}

export default Card

Card.propTypes = {
  title: PropTypes.string, 
  status: PropTypes.number, 
  description: PropTypes.string, 
  priority: PropTypes.number,
  date: PropTypes.string,
  time: PropTypes.string, 
  onCardClick: PropTypes.func
}

const cardStyle = StyleSheet.create({
  container: {
    ...styles.p_10,
    ...styles.m_10,
    borderRadius: 5,
    ...styles.white_bg,
    borderBottomWidth: 0,
    shadowColor: '#d5d5d5',
    shadowOffset: {
      width: 2,
      height: 3
    },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    elevation: 1,
    borderLeftWidth: 3
  },
  title: {
    ...styles.row
  },
  body: {
    ...styles.pt_10,
    ...styles.pb_10
  },
  footer: {
    ...styles.row
  }
})
