import React from 'react'
import PropTypes from 'prop-types'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native'
import Card from './Card'
import styles from '../../../style'
import dimensions from '../../../constants/dimensions'
import colors from '../../../constants/colors'
import VBase from '../../../components/base'

class ListCard extends React.Component {
  render() {
    const { onCardClick, onCardRemove, data } = this.props
    return (
      <FlatList
        ref={ref => this._list = ref}
        data={data}
        keyExtractor={(card) => card.id.toString()}
        renderItem={({ item }) => {
          return (
            <VBase.Swipeable
              key={item.id.toString()}
              onSwipeStart={() => {
                this._list.getScrollResponder().setNativeProps({
                  scrollEnabled: false
                })
              }}
              onSwipeRelease={() => {
                this._list.getScrollResponder().setNativeProps({
                  scrollEnabled: true
                })
              }}
              rightButtons={[
                <TouchableOpacity
                  style={{ flex: 1, ...styles.main_center, ...styles.pl_30, ...styles.red_bg }}
                  onPress={() => onCardRemove(item.id)}>
                  <Ionicons name='md-trash' color={colors.WHITE} size={dimensions.SUPER_EXTRA} />
                </TouchableOpacity>
              ]}>
              <Card {...item} onCardClick={() => onCardClick(item)} />
            </VBase.Swipeable>
          )
        }}>
      </FlatList>
    )
  }
}

export default ListCard

ListCard.propTypes = {
}

ListCard.defaultProps = {
}

const listStyle = StyleSheet.create({
})
