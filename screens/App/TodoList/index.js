import React from 'react';
import { connect } from 'react-redux'
import { View, Text } from 'react-native';
import Header from './Header';
import ListCard from './ListCard'
import styles from '../../../style'
import CreateButton from './CreateButton'
import providers from '../../../providers'
import * as actions from '../../../actions'

class TodoListScreen extends React.Component {
  static navigationOptions = (props) => {
    return {
      header: <Header title='Hey you, This is your to-do list.' />
    }
  }

  componentWillMount = async () => {
    const cards = await providers.card.fetch()
    this.props.fetchCards(cards)
  }

  onCardRemove = async (id) => {
    const result = await providers.card.clear(id)
    if (result) {
      this.props.removeCard(id)
    }
  }

  render() {
    const { cards } = this.props
    return (
      <View style={{ flex: 1, ...styles.lightgray_bg }}>
        {
          cards.length
            ? (
              <ListCard data={cards}
                onCardClick={(data) => this.props.navigation.navigate('TodoDetail', { data })}
                onCardRemove={this.onCardRemove} />
            )
            : (
              <View style={{ ...styles.row, ...styles.w_full, ...styles.main_center }}>
                <Text style={{ fontStyle: 'italic', ...styles.small_txt, ...styles.gray_txt }}>Have no card, click button below to create your card</Text>
              </View>
            )
        }
        <CreateButton onPress={() => this.props.navigation.navigate('TodoDetail')} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    cards: state.cards
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCards: (cards) => dispatch(actions.fetchCards(cards)),
    removeCard: (id) => dispatch(actions.removeCard(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoListScreen)