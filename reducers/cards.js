import * as actionTypes from '../actions/types'

export default cards = (state = [], action) => {
  let _state = JSON.parse(JSON.stringify(state))

  switch (action.type) {
    case actionTypes.FETCH_CARDS:
      _state = action.payload
      return _state

    case actionTypes.ADD_CARD:
      _state.push(action.payload)
      return _state

    case actionTypes.UPDATE_CARD:
      let updIdx = _state.findIndex(c => c.id === action.payload.id)
      if (updIdx === -1) {
        return _state
      }
      _state[updIdx] = action.payload
      return _state

    case actionTypes.REMOVE_CARD:
      let rmIdx = _state.findIndex(c => c.id === action.payload)
      if (rmIdx === -1) {
        return _state
      }
      _state.splice(rmIdx, 1)
      return _state

    default:
      return _state;
  }
}