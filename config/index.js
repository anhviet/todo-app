export default {
  FORMAT_DATE: 'DD/MM/YYYY',
  FORMAT_TIME: 'HH:mm',
  FORMAT_DATE_TIME: 'DD/MM/YYYY HH:mm'
}