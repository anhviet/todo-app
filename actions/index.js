import * as Types from './types'

export const fetchCards = (payload) => {
  return {
    type: Types.FETCH_CARDS,
    payload
  }
}

export const addCard = (payload) => {
  return {
    type: Types.ADD_CARD,
    payload
  }
}

export const updateCard = (payload) => {
  return {
    type: Types.UPDATE_CARD,
    payload
  }
}

export const removeCard = (payload) => {
  return {
    type: Types.REMOVE_CARD,
    payload
  }
}