export const FETCH_CARDS = 'FETCH_CARDS'
export const ADD_CARD = 'ADD_CARD'
export const UPDATE_CARD = 'UPDATE_CARD'
export const REMOVE_CARD = 'REMOVE_CARD'