export default {
  WHITE: '#ffffff',
  BLACK: '#3b434b',
  LIGHT_BLACK: '#414448',
  YELLOW: '#e9d641',
  GREEN: '#5c9f37',
  BLUE: '#0f87dd',
  LIGHT_BLUE: '#9febed',
  RED: '#d6221d',
  LIGHT_RED: '#ffdedd',
  WEIGHT_GRAY: '#777f7b',
  LIGHT_GRAY: '#f9f9f9',
  GRAY: '#bfc5c9',
  BACKDROP: '#0e030380'
};
