import * as Storage from '../utils/SQLStorage'

export const fetch = async () => {
  try {
    // await CardStorage.clearAll()
    let cards = await Storage.fetch()
    return cards
  } catch (e) {
    return []
  }
}

export const add = async (card) => {
  try {
    const result = await Storage.add(card)
    if (result) {
      return card
    }
    return null
  } catch (e) {
    return null
  }
}

export const update = async (card) => {
  try {
    const result = await Storage.update(card)
    if (result) {
      return card
    }
    return null
  } catch (e) {
    return null
  }
}

export const clear = async (id) => {
  try {
    const result = await Storage.clear(id)
    if (result) {
      return id
    }
    return null
  } catch (e) {
    return null
  }
}