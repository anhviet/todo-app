## Install package
> npm install

## Android
npm run android

## iOS
1. Go to ios folder
> pod install

Refer: how to install react-native-sqlite-storage at https://github.com/andpor/react-native-sqlite-storage

2. Back to root folder
> npm run ios

## Build product
### Android
> npm run apk
### iOS
Using xcode
